package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException  {
        try {
            Collections.sort(inputNumbers);             //sorting the numbers for the pyramid
        } catch (NullPointerException|OutOfMemoryError e){
            throw new CannotBuildPyramidException();
        }

        //check if the pyramid can be built from this amount of numbers
        int size=inputNumbers.size();
        int k=1;
        int rows=0;
        while (size>0){
            size-=k;
            k++;
            rows++;
        }
        int cols=2*rows-1;
        if (size<0) throw new CannotBuildPyramidException(); //inputNumbers has less numbers, than necessary

        //build pyramid
        int [][] result = new int[rows][cols]; //empty matrix
        int numPos=0;
        for  (int i = 1;  i <=rows;  i++)      //put the sorted numbers into a pyramid matrix
        {
            int startPos=(rows-i);
            for (int j=0; j<i;j++){
                result[i-1][startPos+2*j]=inputNumbers.get(numPos++);
            }
        }

        return result;
    }


}
