package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement==null) return null;


        ArrayList <String> statementArr= makeStatementArr(statement);
        if (statementArr==null) return null; //invalid numbers

        boolean mode=false; //false - integers only | true - working with doubles
        if(statementArr.remove(statementArr.size()-1).equals("1")) mode=true;


        ArrayList <String> postfixNot=makePostfix(statementArr);             //make postfix notation of expression
        if (postfixNot.contains("(")||postfixNot.contains(")")) return null; //parentheses are forbidden in postfix notation



        int i=0; //calculation in postfix notation
        while (postfixNot.size()>1){
            if (postfixNot.get(i).matches("[+ \\- * /]")){ //met first operation sign

                String operator = new String();
                String operand2 = new String();
                String operand1 = new String();

                try {
                     operator = postfixNot.remove(i);
                     operand2 = postfixNot.remove(i - 1);
                     operand1 = postfixNot.remove(i - 2);
                } catch (ArrayIndexOutOfBoundsException e){ //wrong math symbols order in expression
                    return null;
                }
                String resultOfOperation=new String();

               if (!mode) {
                    try{
                        int op1=Integer.parseInt(operand1);
                        int op2=Integer.parseInt(operand2);
                        resultOfOperation=makeOperation(op1,op2,operator);

                    } catch (IllegalArgumentException e){
                        return null;
                    }
                } else {
                    try{
                        double op1=Double.parseDouble(operand1);
                        double op2=Double.parseDouble(operand2);
                        resultOfOperation=makeOperation(op1,op2,operator);
                    } catch (IllegalArgumentException e){
                        return null;
                    }
                }
                if (resultOfOperation==null) return null;
                postfixNot.add(i-2,resultOfOperation);
                i=0;
            }
            i++;
        }

        return roundUpTo(postfixNot.get(0),4);
    }


    /**
     * Get the priority of arithmetic operation
     * @param operation operator symbol
     * @return priority in byte value from 0 to 3;
     *        -1 means, that number or unsupported operation received
     *
     */
    private byte priority (char operation){
        switch (operation){
            case '(':
                return 0;

            case ')':
                return 1;

            case '+':
            case '-':
                return 2;

            case '*':
            case '/':
                return 3;

            default://unknown operator or number
                return -1;
        }
    }


    /**
     * Round the value of double if it needs to be rounded,
     * otherwise (int or not necessary to round) return original number
     * @param number
     * @param digitsAmount amount of significant digits to be rounded
     * @return string value containing the result
     */
    private String roundUpTo (String number, int digitsAmount){

        if (number.matches(doubleRegex)) {
            int dotPos=number.toString().indexOf('.');
            if ((number.length()-dotPos)<digitsAmount+2) return number;  //nothing to round
            char r=number.charAt(dotPos+digitsAmount+1);
            if(r>52) {  //52='4'
                return(number.substring(0,dotPos+digitsAmount)+ (char)(number.charAt(dotPos+digitsAmount)+1));
            }
            else{
                return (number.substring(0,dotPos+digitsAmount+1));
            }
        } else return number;
    }
    /**
     * Parse the statement from the string to array
     * @param statement first operand
     * @return ArrayList containing the statement operators and numbers,
     *         null if the statement is empty or has invalid number
     */
    private ArrayList<String> makeStatementArr(String statement){
        if (statement.isEmpty()) return null;
        ArrayList <String> statementArr= new ArrayList<>();
        
        boolean digReadState=false;
        boolean mode=false; //false - integers only | true - working with doubles
        int startDigPos=0;

        for (int i = 0; i <statement.length() ; i++) {
            char c=statement.charAt(i);
            if (priority(c)!=-1){//operation or parentheses
                if (digReadState){//finish number reading
                    String newNumber=statement.substring(startDigPos,i);
                    if (newNumber.matches(doubleRegex)) {
                        mode=true;
                    }
                    else if (newNumber.matches(intRegex)) {
                    }
                    else return null; //invalidNumber

                    statementArr.add(newNumber);
                    digReadState=false;
                }
                statementArr.add(Character.toString(c));

            }else{
                if (!digReadState){
                    startDigPos=i;
                    digReadState=true;
                }
            }
        }
        if (digReadState){
            String newNumber=statement.substring(startDigPos,statement.length());
            statementArr.add(newNumber);
            if (newNumber.matches(doubleRegex)) {
                mode=true;
            }
        }

        if (mode) statementArr.add("1");
        else statementArr.add("0");
        return statementArr;
    }


    /**
     * Make postfix notation (Reverse Polish notation)
     * from the statement
     * @param statementArray statement
     * @return ArrayList containing operand and operators of statement in postfix order,
     *          null if statement has invalid parentheses order
     */
    private ArrayList<String> makePostfix(ArrayList <String> statementArray){
        ArrayList <String> postfixNot=new ArrayList<>();
        LinkedList <String> operationStack=new LinkedList<>();

        for (String str:statementArray) {
            byte currentPriority=priority(str.charAt(0));

            if (currentPriority==-1) {//received a number, concat to postfix notation array
                postfixNot.add(str);
            }
            else{
                if(operationStack.isEmpty()){                            //always add operation onto empty stack
                    operationStack.addFirst(str);
                }

                else  if(currentPriority==0) operationStack.addFirst(str);   //always push "(" onto stack


                else if(currentPriority==1)  {                           //get ")", so pop everything after "(" from the stack
                    int lastIndexOfPar=operationStack.indexOf("(");
                    if (lastIndexOfPar==-1) return null;                //wrong parentheses format
                    else {
                        for (int j=0;j<lastIndexOfPar;j++)   postfixNot.add(operationStack.removeFirst());
                        operationStack.removeFirst();       //remove "(" from the stack
                    }
                }


                else {                                                // pop the operations with higher or equal priority

                    Iterator iter=operationStack.iterator();
                    while (iter.hasNext()){
                        String popedOper=(String)iter.next();
                        if (priority(popedOper.charAt(0))>=currentPriority) {postfixNot.add(operationStack.getFirst());iter.remove();}
                        else  break;
                    }
                    operationStack.addFirst(str);
                }
            }
        }

        if (!operationStack.isEmpty()) postfixNot.addAll(operationStack);
        return postfixNot;
    }

    /**
     * Evaluate statement with 2 operands
     * @param op1 first operand
     * @param op2 second operand
     * @param operator operator symbol
     * @return string value containing the result of operation
     *          null in case of division by zero or unsupported operation
     */
    private String makeOperation (int op1, int op2, String operator){
        switch (operator){
            case "+":
                return String.valueOf(op1+op2);
            case "-":
                return String.valueOf(op1-op2);
            case "*":
                return String.valueOf(op1*op2);
            case "/":
                if (op2==0) return null;
                if (op1%op2!=0) return String.valueOf(1.0*op1/op2);
                else return String.valueOf(op1/op2);
            default:return null;
        }
    }
    private String makeOperation (double op1, double op2, String operator){
        switch (operator){
            case "+":
                return String.valueOf(op1+op2);
            case "-":
                return String.valueOf(op1-op2);
            case "*":
                return String.valueOf(op1*op2);
            case "/":
                if (op2==0.0) return null;
                return String.valueOf(op1/op2);
            default:return null;
        }
    }


    String doubleRegex="^[\\\\+\\\\-]{0,1}[0-9]+[\\\\.\\\\,][0-9]+$";
    String intRegex="^[0-9]*[1-9]+[0-9]*$";

  
}