package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException{
              
       //check size and null
        if((x==null)||y==null) throw  new IllegalArgumentException();
        if (x.isEmpty()) return true;                   //always able to get empty sequence from everything not null
        if (x.size()>y.size()) return false;            //impossible to get a bigger sequence


        int i=0;
        int j=0;
        while ((j<y.size())&&(i<x.size())){
            if (x.get(i).equals(y.get(j))) i++;
            j++;
        }

        if (i==x.size()) return true;
        return false;
    }
}
